/*
    Macro Board App is a Windows program to handle the MacroBoard USB HID Device
    It can launch programs, manage audio devices, control the volume etc...
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string>
#include <fstream>
#include <vector>

#include <windows.h>
#include <shellapi.h>
#include <Mmsystem.h>

#include <thread>

#include <conio.h>

#include "hid.h"

#include "resource.h"

//Tray App Setup
UINT WM_TASKBAR = 0;
HWND Hwnd;
HMENU Hmenu;
NOTIFYICONDATA notifyIconData;
TCHAR szTIP[64] = TEXT("Macro Board");
char szClassName[] = "Control your computer";

LRESULT CALLBACK WindowProcedure(HWND, UINT, WPARAM, LPARAM);

//Toggle States
bool macroState1 = false;
bool macroState2 = false;
bool macroState3 = false;

// used in a while loop
bool rawHIDKeepLooping = true;

std::vector <std::string> selectedApps1;
std::vector <std::string> selectedApps2;

std::vector <std::string> launchApps1;
std::vector <std::string> launchApps2;
std::vector <std::string> launchApps3;

std::vector <std::string> audioDevices;

#define AppsGroupSelect (macroState3 ? selectedApps1 : selectedApps2)


// Declaring Functions
void minimize();

void restore();

void InitNotifyIconData();

bool initRawHID();

void pingRawHID();

bool computeRawMessage();

void listenRawHID();

bool importConfiguration();

bool importTxtFileToStringVector(const std::string &fileName, std::vector <std::string>& stringVector);

//init rawHID protocole & device
bool initRawHID()
{
    // C-based example is 16C0:0480:FFAB:0200
    int result = rawhid_open(1, 0x16C0, 0x0480, 0xFFAB, 0x0200); 

    if (result <= 0) {
        // Arduino-based example is 16C0:0486:FFAB:0200
        result = rawhid_open(1, 0x16C0, 0x0486, 0xFFAB, 0x0200);
        if (result <= 0) {
            printf("no rawhid device found\n");
            return false;
        }
    }
    printf("found rawhid device\n");
    rawhid_send(0, (byte*)"MB?", 64, 100); // sends a message to ask the current toggle buttons states
    return true;
}

void pingRawHID()
{
    while (rawHIDKeepLooping)
    {
        Sleep(100);
        if (rawhid_send(0, (byte*)"Ping", 64, 100) == -1)
        {
            rawhid_close(0);
            initRawHID();
        }
    }
}

//RawHID interp
bool computeRawMessage()
{
    int numRead;
    char buf[64];
	// check if any Raw HID packet has arrived
	numRead = rawhid_recv(0, buf, 64, 220);
	if (numRead < 0) {
		printf("\nerror reading, device went offline\n");
		rawhid_close(0);
		return false;
	}
	if (numRead > 0) {
		printf("\n");
		printf(buf);
		printf("\n");

		if (strcmp(buf,"Rotary_Left-") == 0)
		{
            WinExec("nircmd.exe changesysvolume -2000", SW_HIDE);
            PlaySound(TEXT("sound_tap.wav"), NULL, SND_FILENAME | SND_ASYNC);
		}

		if (strcmp(buf, "Rotary_Left+") == 0)
		{
            WinExec("nircmd.exe changesysvolume 2000", SW_HIDE);
            PlaySound(TEXT("sound_tap.wav"), NULL, SND_FILENAME | SND_ASYNC);
		}

		if (strcmp(buf, "Rotary_Left") == 0)
		{
            WinExec("nircmd.exe mutesysvolume 2", SW_HIDE);
            PlaySound(TEXT("sound_tap.wav"), NULL, SND_FILENAME | SND_ASYNC);
		}


		if (strcmp(buf, "Rotary_Middle-") == 0)
		{
            WinExec("nircmd.exe changeappvolume focused -.1", SW_HIDE);
            PlaySound(TEXT("sound_tap.wav"), NULL, SND_FILENAME | SND_ASYNC);
		}

		if (strcmp(buf, "Rotary_Middle+") == 0)
		{
            WinExec("nircmd.exe changeappvolume focused .1", SW_HIDE);
            PlaySound(TEXT("sound_tap.wav"), NULL, SND_FILENAME | SND_ASYNC);
		}

		if (strcmp(buf, "Rotary_Middle") == 0)
		{
            WinExec("nircmd.exe muteappvolume focused 2", SW_HIDE);
            PlaySound(TEXT("sound_tap.wav"), NULL, SND_FILENAME | SND_ASYNC);
		}


		if (strcmp(buf, "Rotary_Right-") == 0)
		{
            for (const std::string& text : AppsGroupSelect)
            {
                std::string cmd = "nircmd.exe changeappvolume \"" + text + "\" -.1";
                WinExec(cmd.c_str(), SW_HIDE);
            }
            PlaySound(TEXT("sound_tap.wav"), NULL, SND_FILENAME | SND_ASYNC);
		}

		if (strcmp(buf, "Rotary_Right+") == 0)
		{
            for (const std::string& text : AppsGroupSelect)
            {
                std::string cmd = "nircmd.exe changeappvolume \"" + text + "\" .1";
                WinExec(cmd.c_str(), SW_HIDE);
            }
            PlaySound(TEXT("sound_tap.wav"), NULL, SND_FILENAME | SND_ASYNC);
		}

		if (strcmp(buf, "Rotary_Right") == 0)
        {
            for (const std::string& text : AppsGroupSelect)
            {
                std::string cmd = "nircmd.exe muteappvolume \"" + text + "\" 2";
                WinExec(cmd.c_str(), SW_HIDE);
            }
            PlaySound(TEXT("sound_tap.wav"), NULL, SND_FILENAME | SND_ASYNC);
		}


		if (strcmp(buf, "Toggle_Left-") == 0)
		{
            std::string cmd = "nircmd.exe setdefaultsounddevice \"Headset\"";
            if (audioDevices.size() > 0)
            {
                cmd = "nircmd.exe setdefaultsounddevice " + audioDevices[0];
            }
            WinExec(cmd.c_str(), SW_HIDE);
            macroState1 = false;
		}

		if (strcmp(buf, "Toggle_Left+") == 0)
		{
            std::string cmd = "nircmd.exe setdefaultsounddevice \"Speakers\"";
            if (audioDevices.size() > 1)
            {
                cmd = "nircmd.exe setdefaultsounddevice " + audioDevices[1];
            }
            WinExec(cmd.c_str(), SW_HIDE);
            macroState1 = true;
		}


		if (strcmp(buf, "Toggle_Middle-") == 0)
		{
            macroState2 = false;
		}

		if (strcmp(buf, "Toggle_Middle+") == 0)
		{
            macroState2 = true;
		}


		if (strcmp(buf, "Toggle_Right-") == 0)
		{
            macroState3 = false;
		}

		if (strcmp(buf, "Toggle_Right+") == 0)
		{
            macroState3 = true;
		}


		if (strcmp(buf, "Push_Left") == 0)
		{
            if (macroState2)
            {
                for (const std::string& text : launchApps1)
                {
                    ShellExecute(NULL, "open", text.c_str(), NULL, NULL, SW_SHOWNORMAL);
                }
            }
            else 
            {
                keybd_event(VK_MEDIA_PREV_TRACK, 0, KEYEVENTF_EXTENDEDKEY, NULL);
            }
            PlaySound(TEXT("sound_tap.wav"), NULL, SND_FILENAME | SND_ASYNC);
		}


		if (strcmp(buf, "Push_Middle") == 0)
		{
            if (macroState2)
            {
                for (const std::string& text : launchApps2)
                {
                    ShellExecute(NULL, "open", text.c_str(), NULL, NULL, SW_SHOWNORMAL);
                }
            }
            else
            {
                keybd_event(VK_MEDIA_PLAY_PAUSE, 0, KEYEVENTF_EXTENDEDKEY, NULL);
            }
            PlaySound(TEXT("sound_tap.wav"), NULL, SND_FILENAME | SND_ASYNC);
		}


		if (strcmp(buf, "Push_Right") == 0)
		{
            if (macroState2)
            {
                for (const std::string& text : launchApps3)
                {
                    ShellExecute(NULL, "open", text.c_str(), NULL, NULL, SW_SHOWNORMAL);
                }
            }
            else
            {
                keybd_event(VK_MEDIA_NEXT_TRACK, 0, KEYEVENTF_EXTENDEDKEY, NULL);
            }
            PlaySound(TEXT("sound_tap.wav"), NULL, SND_FILENAME | SND_ASYNC);
		}

        return true;
	}
}

// RawHID interp Loop
void listenRawHID()
{
    while (rawHIDKeepLooping)
    {
        computeRawMessage();
    }

}

// imports list of apps (programs) to group for volume control
bool importConfiguration()
{
    selectedApps1.clear();
    selectedApps2.clear();

    launchApps1.clear();
    launchApps2.clear();
    launchApps3.clear();

    audioDevices.clear();

    importTxtFileToStringVector("selectAppGroup.1.txt", selectedApps1);
    importTxtFileToStringVector("selectAppGroup.2.txt", selectedApps2);

    importTxtFileToStringVector("launchAppGroup.1.txt", launchApps1);
    importTxtFileToStringVector("launchAppGroup.2.txt", launchApps2);
    importTxtFileToStringVector("launchAppGroup.3.txt", launchApps3);

    importTxtFileToStringVector("audioDevices.txt", audioDevices);

    return true;
}

bool importTxtFileToStringVector(const std::string &fileName, std::vector <std::string> &stringVector)
{
    std::ifstream infile(fileName);
    std::string str;

    while (std::getline(infile, str))
    {
        if (str.size() > 0)
            stringVector.push_back(str);
    }

    return true;
}


int WINAPI WinMain(HINSTANCE hThisInstance,
    HINSTANCE hPrevInstance,
    LPSTR lpszArgument,
    int nCmdShow)
{
    /* This is the handle for our window */
    MSG messages;            /* Here messages to the application are saved */
    WNDCLASSEX wincl;        /* Data structure for the windowclass */
    WM_TASKBAR = RegisterWindowMessageA("TaskbarCreated");
    /* The Window structure */
    wincl.hInstance = hThisInstance;
    wincl.lpszClassName = szClassName;
    wincl.lpfnWndProc = WindowProcedure;      /* This function is called by windows */
    wincl.style = CS_DBLCLKS;                 /* Catch double-clicks */
    wincl.cbSize = sizeof(WNDCLASSEX);

    /* Use default icon and mouse-pointer */
    wincl.hIcon = LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(ICO1));
    wincl.hIconSm = LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(ICO1));
    wincl.hCursor = LoadCursor(NULL, IDC_ARROW);
    wincl.lpszMenuName = NULL;                 /* No menu */
    wincl.cbClsExtra = 0;                      /* No extra bytes after the window class */
    wincl.cbWndExtra = 0;                      /* structure or the window instance */
    wincl.hbrBackground = (HBRUSH)(CreateSolidBrush(RGB(255, 255, 255)));
    /* Register the window class, and if it fails quit the program */
    if (!RegisterClassEx(&wincl))
        return 0;

    /* The class is registered, let's create the program*/
    Hwnd = CreateWindowEx(
        0,                   /* Extended possibilites for variation */
        szClassName,         /* Classname */
        szClassName,       /* Title Text */
        WS_OVERLAPPEDWINDOW, /* default window */
        CW_USEDEFAULT,       /* Windows decides the position */
        CW_USEDEFAULT,       /* where the window ends up on the screen */
        256,                 /* The programs width */
        128,                 /* and height in pixels */
        HWND_DESKTOP,        /* The window is a child-window to desktop */
        NULL,                /* No menu */
        hThisInstance,       /* Program Instance handler */
        NULL                 /* No Window Creation data */
    );

    /*Initialize the NOTIFYICONDATA structure only once*/
    InitNotifyIconData();

    ShowWindow(Hwnd, nCmdShow);

    minimize();

    /*
        MacroBoard Main
        ///////////////
    */

    importConfiguration();

    initRawHID();

    auto th1 = std::thread(listenRawHID);

    auto th2 = std::thread(pingRawHID);

    /*
        ///////////////
    */


    /* Run the message loop. It will run until GetMessage() returns 0 */
    while (GetMessage(&messages, NULL, 0, 0))
    {
        /* Translate virtual-key messages into character messages */
        TranslateMessage(&messages);
        /* Send message to WindowProcedure */
        DispatchMessage(&messages);
    }

    rawHIDKeepLooping = false;
    th1.join();
    th2.join();

    rawhid_close(0);

    return messages.wParam;
}


/*  This function is called by the Windows function DispatchMessage()  */

LRESULT CALLBACK WindowProcedure(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{

    if (message == WM_TASKBAR && !IsWindowVisible(Hwnd))
    {
        minimize();
        return 0;
    }

    switch (message)                  /* handle the messages */
    {
    case WM_ACTIVATE:
        Shell_NotifyIcon(NIM_ADD, &notifyIconData);
        break;
    case WM_CREATE:

        ShowWindow(Hwnd, SW_HIDE);
        Hmenu = CreatePopupMenu();
        AppendMenu(Hmenu, MF_STRING, ID_TRAY_EXIT, TEXT("Exit Macro Board"));

        break;

    case WM_SYSCOMMAND:
        /*In WM_SYSCOMMAND messages, the four low-order bits of the wParam parameter
        are used internally by the system. To obtain the correct result when testing the value of wParam,
        an application must combine the value 0xFFF0 with the wParam value by using the bitwise AND operator.*/

        switch (wParam & 0xFFF0)
        {
        case SC_MINIMIZE:
        case SC_CLOSE:
            minimize();
            return 0;
            break;
        }
        break;


        // Our user defined WM_SYSICON message.
    case WM_SYSICON:
    {

        switch (wParam)
        {
        case ID_TRAY_APP_ICON:
            SetForegroundWindow(Hwnd);

            break;
        }


        if (lParam == WM_LBUTTONUP)
        {

            restore();
        }
        else if (lParam == WM_RBUTTONDOWN)
        {
            // Get current mouse position.
            POINT curPoint;
            GetCursorPos(&curPoint);
            SetForegroundWindow(Hwnd);

            // TrackPopupMenu blocks the app until TrackPopupMenu returns

            UINT clicked = TrackPopupMenu(Hmenu, TPM_RETURNCMD | TPM_NONOTIFY, curPoint.x, curPoint.y, 0, hwnd, NULL);



            SendMessage(hwnd, WM_NULL, 0, 0); // send benign message to window to make sure the menu goes away.
            if (clicked == ID_TRAY_EXIT)
            {
                // quit the application.
                Shell_NotifyIcon(NIM_DELETE, &notifyIconData);
                PostQuitMessage(0);
            }
        }
    }
    break;

    // intercept the hittest message..
    case WM_NCHITTEST:
    {
        UINT uHitTest = DefWindowProc(hwnd, WM_NCHITTEST, wParam, lParam);
        if (uHitTest == HTCLIENT)
            return HTCAPTION;
        else
            return uHitTest;
    }

    case WM_CLOSE:

        minimize();
        return 0;
        break;

    case WM_DESTROY:

        PostQuitMessage(0);
        break;

    }

    return DefWindowProc(hwnd, message, wParam, lParam);
}


void minimize()
{
    // hide the main window
    ShowWindow(Hwnd, SW_HIDE);
}


void restore()
{
    ShowWindow(Hwnd, SW_SHOW);
}

void InitNotifyIconData()
{
    memset(&notifyIconData, 0, sizeof(NOTIFYICONDATA));

    notifyIconData.cbSize = sizeof(NOTIFYICONDATA);
    notifyIconData.hWnd = Hwnd;
    notifyIconData.uID = ID_TRAY_APP_ICON;
    notifyIconData.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
    notifyIconData.uCallbackMessage = WM_SYSICON; //Set up our invented Windows Message
    notifyIconData.hIcon = (HICON)LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(ICO1));
    strncpy_s(notifyIconData.szTip, szTIP, sizeof(szTIP));
}
